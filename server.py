import base64
import json

entity_properties = {
    # "generic.movementSpeed": 0.699999988079071,
    # "generic.flyingSpeed": 0.4000000059604645
    "generic.movementSpeed": 0.2,
    "generic.flyingSpeed": 0.1
}

protocol = 404
version = '1.13.2'
brand = 'Ops'
player_limit = 50
logging_in = {}
players = {}
with open("resources/server-icon.png", "rb") as image_file:
    icon = base64.b64encode(image_file.read()).decode()
with open('items.json') as f:
    items = json.load(f)
with open('blocks.json') as f:
    blocks = json.load(f)
item_id_to_block_id = {}


def get_player_count():
    return len(players)


def init_item_id_to_block_id_map():
    for key, value in items.items():
        item_name = key
        item_id = value["protocol_id"]
        if item_name not in blocks:
            continue
        block = blocks[item_name]
        for state in block["states"]:
            if "default" not in state:
                continue
            if state["default"]:
                block_id = state["id"]
                item_id_to_block_id[item_id] = block_id

# class Server(object):
#
#     def __init__(self):
#         self.players = []
#
#     def get_players(self):
#         return self.players
