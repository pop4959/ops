import socket
import json
import time
import uuid
import logging
import struct
import random
from threading import Thread

import util
import packet
import receiver
import server
import network
from network import Width, Value, Position
from player import Player

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
port = 25565


# Pack a list of 16x16x16 blocks into longs, LSB to MSB, with remaining data put into next long
def pack_blocks(bits_per_block, blocks):
    longs = []
    current_offset = 0
    current_long = 0
    for block in blocks:
        current_long |= (block << current_offset)
        overflow = current_offset + bits_per_block - 64
        if overflow < 0:
            current_offset += bits_per_block
        else:
            current_long &= 0xFFFFFFFFFFFFFFFF
            longs.append(current_long)
            current_long = 0
            remaining_bits = bits_per_block - overflow
            current_long |= (block >> remaining_bits)
            current_offset = overflow
    if current_long != 0:  # Might only be debug since normally it should come out to an even number of longs
        longs.append(current_long)
    return longs


def longs_to_byte_array(longs):
    data = b''
    for long in longs:
        data += struct.pack('>Q', long)
    return data


# A single chunk section, using direct palette, assuming ground_up_continuous / full chunk, in overworld
class SingleChunkSection:
    def __init__(self, bits_per_block, data_array, block_light, sky_light, biomes):
        self.bits_per_block = bits_per_block
        self.data_array_length = 896
        self.data_array = data_array
        self.block_light = block_light
        self.sky_light = sky_light
        self.biomes = biomes

    def __len__(self):
        return 1 + 2 + 896 * 8 + 2048 + 2048 + 256 * 4
        # 1 byte bits_per_block
        # ceil(log2(896)/8) = 2 byte data_array_length varint
        # data_array_length*8_bytes_per_long bytes
        # 4096 blocks * 1/2 byte per block
        # 4096 blocks * 1/2 byte per block
        # 256 biomes * 4 bytes per biome


def handle_play(client_socket):
    packet.send_join_game(client_socket, Value(Width.INT, 0), Value(Width.UNSIGNED_BYTE, 1), Value(Width.INT, 0),
                          Value(Width.UNSIGNED_BYTE, 0), Value(Width.UNSIGNED_BYTE, 0), '0', False)
    packet.send_plugin_message(client_socket, 'minecraft:brand', server.brand)
    packet.send_server_difficulty(client_socket, Value(Width.UNSIGNED_BYTE, 0))
    packet.send_spawn_position(client_socket, Position((0, 0, 0)))
    packet.send_player_abilities(client_socket, Value(Width.BYTE, 6),
                                 Value(Width.FLOAT, server.entity_properties["generic.movementSpeed"]),
                                 Value(Width.FLOAT, server.entity_properties["generic.flyingSpeed"]))
    packet.send_player_position_and_look(client_socket, Value(Width.DOUBLE, 0), Value(Width.DOUBLE, 5),
                                         Value(Width.DOUBLE, 0), Value(Width.FLOAT, 0), Value(Width.FLOAT, 0),
                                         Value(Width.BYTE, 0), 0)
    preset_data = 0b10000000000000100000000000001000000000000010000000000000
    preset_bytes = bytes(
        [(preset_data & 0xF000000) >> 56, (preset_data & 0x0F00000) >> 48, (preset_data & 0x00F0000) >> 32,
         (preset_data & 0x000F000) >> 24, (preset_data & 0x0000F00) >> 16, (preset_data & 0x00000F0) >> 8,
         preset_data & 0x000000F])
    # data_array = struct.pack('<{}s'.format(896 * 8), bytes([1]))
    # data_array = struct.pack('<{}s'.format(896 * 8), longs_to_byte_array(pack_blocks(14, [1083, 1084, 1085, 1086, 1087,
    #                                                                                       1088, 1089, 1090, 1091, 1092,
    #                                                                                       1093, 1094, 1095, 1096, 1097,
    #                                                                                       1098] * 16 * 16)))
    data_array = struct.pack('<{}s'.format(896 * 8), longs_to_byte_array(pack_blocks(14, [9] * 16 * 16)))
    data_array2 = struct.pack('<{}s'.format(896 * 8), longs_to_byte_array(pack_blocks(14, [0] * 16 * 16 * 16)))
    block_light = struct.pack('<{}s'.format(2048), bytes([255] * 2048))
    sky_light = struct.pack('<{}s'.format(2048), bytes())
    biomes = struct.pack('<{}s'.format(256 * 4), bytes())
    chunk_sect = SingleChunkSection(14, data_array, block_light, sky_light, biomes)

    for i in range(-8, 8):
        for j in range(-8, 8):
            # We are sending this packet manually, so we need to send the size and packet ID
            packet_size = 1 + 4 + 4 + 1 + 1 + 2 + len(chunk_sect) + 1
            network.encode_varint(client_socket, packet_size)
            network.encode_varint(client_socket, packet.PacketID.SEND_CHUNK_DATA)

            network.encode_int(client_socket, Value(Width.INT, i))  # Chunk X
            network.encode_int(client_socket, Value(Width.INT, j))  # Chunk Z
            network.encode_boolean(client_socket, True)  # Ground up continuous
            network.encode_varint(client_socket, 1)  # Primary Bit Mask
            network.encode_varint(client_socket, len(chunk_sect))  # Size

            # Data
            network.encode_unsigned_byte(client_socket,
                                         Value(Width.UNSIGNED_BYTE, chunk_sect.bits_per_block))  # Bits Per Block
            network.encode_varint(client_socket, chunk_sect.data_array_length)  # Data Array Length
            network.encode_byte_array(client_socket,
                                      chunk_sect.data_array if -4 <= i <= 3 and -4 <= j <= 3 else data_array2)  # Data Array
            network.encode_byte_array(client_socket, chunk_sect.block_light)  # Block Light
            network.encode_byte_array(client_socket, chunk_sect.sky_light)  # Sky Light
            network.encode_byte_array(client_socket, chunk_sect.biomes)  # Biomes

            network.encode_varint(client_socket, 0)  # Number of block entities

    #  packet.send_chunk_data(0, 0, True, 1, len(data), data, 0)

    server.players[client_socket] = server.logging_in[client_socket]
    del server.logging_in[client_socket]

    for player_socket in server.players:
        if player_socket is not client_socket:
            packet.send_player_list_item(player_socket, 0, 1, server.players[client_socket].uuid,
                                         server.players[client_socket].username, 0, None, None,
                                         None, 1, 0, False)

    for player_socket in server.players:
        packet.send_player_list_item(client_socket, 0, 1, server.players[player_socket].uuid,
                                     server.players[player_socket].username, 0, None, None,
                                     None, 1, 0, False)

    # for eid, player_socket in enumerate(server.players):
    #     if player_socket is not client_socket:
    #         packet.send_spawn_player(client_socket, eid, server.players[player_socket].uuid, Value(Width.DOUBLE, 0),
    #                                  Value(Width.DOUBLE, 5), Value(Width.DOUBLE, 0), Value(Width.ANGLE, 0),
    #                                  Value(Width.ANGLE, 0), Value(Width.UNSIGNED_BYTE, 0xFF))

    #    packet.send_player_list_item(player_socket, 0, 1, server.players[client_socket].uuid,
    #                                 server.players[client_socket].username, 0, None, None, None, 1, 0, False)

    packet.send_boss_bar(client_socket, uuid.UUID(int=0x00000000000000010000000000000002), 0,
                         json.dumps({"text": "Hi there, " + server.players[client_socket].username + "!"}),
                         network.Value(network.Width.FLOAT, 1), 0, 0, network.Value(network.Width.UNSIGNED_BYTE, 0))

    receiver.listen(client_socket)


def handle_status(client_socket):
    packet_size, packet_id = packet.recv_request(client_socket)
    player_list = []
    for player in server.players:
        player_list.append({"name": server.players[player].username, "id": str(server.players[player].uuid)})
    response = {"description": {"text": "xxxxx    ", "bold": "true", "color": "gray", "obfuscated": "true",
                                "extra": [{"text": "Custom Server", "bold": "true", "color": "dark_green",
                                           "obfuscated": "false"},
                                          {"text": "    xxxxx\n", "bold": "true", "color": "gray",
                                           "obfuscated": "true"},
                                          {"text": "xxxxx ", "bold": "true", "color": "gray", "obfuscated": "true"},
                                          {"text": "Currently Under Development", "bold": "true", "color": "dark_aqua",
                                           "obfuscated": "false"},
                                          {"text": " xxxxx", "bold": "true", "color": "gray", "obfuscated": "true"}]},
                "version": {"name": server.brand + " " + server.version, "protocol": server.protocol},
                "players": {"max": server.player_limit, "online": server.get_player_count(), "sample": player_list},
                "favicon": "data:image/png;base64," + server.icon}
    packet.send_response(client_socket, json.dumps(response))
    packet_size, packet_id, ping = packet.recv_ping(client_socket)
    packet.send_pong(client_socket, Value(Width.LONG, ping))
    logging.info("Received server list ping from " + str(socket.gethostbyname(socket.gethostname())))


def handle_login(client_socket):
    packet_size, packet_id, player_username = packet.recv_login_start(client_socket)
    player_uuid = uuid.uuid4()
    packet.send_login_success(client_socket, str(player_uuid), player_username)
    server.logging_in[client_socket] = Player(client_socket, player_uuid, username=player_username)
    logging.info(player_username + " (" + str(player_uuid) + ") logged in")
    handle_play(client_socket)


def handle_connection(client_socket):
    try:
        packet_size, packet_id, protocol_version, server_address, server_port, next_state = packet.recv_handshake(
            client_socket)
        if next_state == 1:
            handle_status(client_socket)
        elif next_state == 2:
            handle_login(client_socket)
    except Exception:
        logging.warning("Exception occurred! Try to remove a player now")
        network._remove_player(client_socket)
        client_socket.shutdown(2)
        client_socket.close()
        exit()


def main():
    server.init_item_id_to_block_id_map()
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('0.0.0.0', port))
    server_socket.listen(server.player_limit)
    while 1:
        (client_socket, address) = server_socket.accept()
        Thread(target=handle_connection, args=(client_socket,)).start()


if __name__ == '__main__':
    main()
