import logging
from urllib.request import urlopen
import urllib
import json


def get_uuid_skin(uuid):
    try:
        response = urlopen("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid.strip("-")).read().decode('utf-8')
        d = json.loads(response)
        return d
    except urllib.error.HTTPError:
        logging.info("[get_uuid_skin] HTTPError for " + uuid + ", usually a rate limit issue. 1 call per minute per UUID ")
    except ValueError:
        logging.debug("[get_uuid_skin] JSON Decode error for " + uuid)


def get_mc_uuid(player_name):
    try:
        response = urlopen("https://api.mojang.com/users/profiles/minecraft/" + player_name).read().decode('utf-8')
        d = json.loads(response)
        return d
    except urllib.error.HTTPError:
        logging.info("[get_mc_uuid] HTTPError for " + player_name + ", usually a rate limit issue. 1 call per minute per UUID ")
    except ValueError:
        logging.debug("[get_mc_uuid] JSON Decode error for " + player_name)
