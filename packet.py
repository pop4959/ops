import logging
import uuid
from enum import IntEnum
from math import log

import network


# Packet IDs


class PacketID(IntEnum):
    # Handshaking (Serverbound)
    RECV_HANDSHAKE = 0x00
    # Play (Clientbound)
    SEND_SPAWN_PLAYER = 0x05
    SEND_BLOCK_CHANGE = 0x0B
    SEND_BOSS_BAR = 0x0C
    SEND_SERVER_DIFFICULTY = 0x0D
    SEND_CHAT_MESSAGE = 0x0E
    SEND_PLUGIN_MESSAGE = 0x19
    SEND_DISCONNECT_PLAY = 0x1B
    SEND_KEEP_ALIVE = 0x21
    SEND_CHUNK_DATA = 0x22
    SEND_JOIN_GAME = 0x25
    SEND_PLAYER_ABILITIES = 0x2E
    SEND_PLAYER_LIST_ITEM = 0x30
    SEND_PLAYER_POSITION_AND_LOOK = 0x32
    SEND_SPAWN_POSITION = 0x49
    # Play (Serverbound)
    RECV_TELEPORT_CONFIRM = 0x00
    RECV_CHAT_MESSAGE = 0x02
    RECV_CLIENT_STATUS = 0x03
    RECV_KEEP_ALIVE = 0x0E
    RECV_PLAYER_POSITION = 0x10
    RECV_PLAYER_POSITION_AND_LOOK = 0x11
    RECV_PLAYER_LOOK = 0x12
    RECV_PLAYER_DIGGING = 0x18
    RECV_HELD_ITEM_CHANGE = 0x21
    RECV_CREATIVE_INVENTORY_ACTION = 0x24
    RECV_ANIMATION = 0x27
    RECV_PLAYER_BLOCK_PLACEMENT = 0x29
    # Status (Clientbound)
    SEND_RESPONSE = 0x00
    SEND_PONG = 0x01
    # Status (Serverbound)
    RECV_REQUEST = 0x00
    RECV_PING = 0x01
    # Login (Clientbound)
    SEND_LOGIN_SUCCESS = 0x02
    # Login (Serverbound)
    RECV_LOGIN_START = 0x00


# Decorators


def encode_packet_header(packet_id):
    def decorator_packet_function(packet_function):
        def mutated_packet_function(*args, **kwargs):
            socket = args[0]
            params = args[1:]
            network.encode_varint(socket, _packet_size(packet_id, *params))
            network.encode_varint(socket, packet_id)
            packet_function(*args, **kwargs)

        return mutated_packet_function

    return decorator_packet_function


# Handshaking (Serverbound)


def recv_handshake(socket):
    packet_size = network.decode_varint(socket)
    packet_id = network.decode_varint(socket)
    protocol_version = network.decode_varint(socket)
    server_address = network.decode_string(socket)
    server_port = network.decode_unsigned_short(socket)
    next_state = network.decode_varint(socket)
    return packet_size, packet_id, protocol_version, server_address, server_port, next_state


# Play (Clientbound)


# TODO: didn't implement eid properly or metadata
@encode_packet_header(PacketID.SEND_SPAWN_PLAYER)
def send_spawn_player(socket, eid, player_uuid, x, y, z, yaw, pitch, metadata):
    network.encode_varint(socket, eid)
    network.encode_uuid(socket, player_uuid)
    network.encode_double(socket, x)
    network.encode_double(socket, y)
    network.encode_double(socket, z)
    # Actually angles
    network.encode_angle(socket, yaw)
    network.encode_angle(socket, pitch)
    # Just send 0xff for now to mark as end of metadata
    network.encode_unsigned_byte(socket, metadata)


@encode_packet_header(PacketID.SEND_BLOCK_CHANGE)
def send_block_change(socket, location, block_id):
    network.encode_position(socket, location)
    network.encode_varint(socket, block_id)


@encode_packet_header(PacketID.SEND_BOSS_BAR)
def send_boss_bar(socket, bar_uuid, action, chat, health, color, division, flags):
    network.encode_uuid(socket, bar_uuid)
    network.encode_varint(socket, action)
    network.encode_string(socket, chat)
    network.encode_float(socket, health)
    network.encode_varint(socket, color)
    network.encode_varint(socket, division)
    network.encode_unsigned_byte(socket, flags)


@encode_packet_header(PacketID.SEND_SERVER_DIFFICULTY)
def send_server_difficulty(socket, difficulty):
    network.encode_unsigned_byte(socket, difficulty)


@encode_packet_header(PacketID.SEND_CHAT_MESSAGE)
def send_chat_message(socket, chat_message, position):
    network.encode_string(socket, chat_message)
    network.encode_unsigned_byte(socket, position)


@encode_packet_header(PacketID.SEND_PLUGIN_MESSAGE)
def send_plugin_message(socket, channel, data):
    network.encode_string(socket, channel)
    network.encode_string(socket, data)


@encode_packet_header(PacketID.SEND_DISCONNECT_PLAY)
def send_disconnect(socket, reason):
    network.encode_string(socket, reason)
    socket.shutdown(2)
    socket.close()


@encode_packet_header(PacketID.SEND_KEEP_ALIVE)
def send_keep_alive(socket, ping):
    network.encode_long(socket, ping)


# TODO
@encode_packet_header(PacketID.SEND_CHUNK_DATA)
def send_chunk_data(socket, chunk_x, chunk_z, ground_up_continuous, primary_bit_mask, size, data,
                    number_of_block_entities):
    network.encode_int(socket, chunk_x)
    network.encode_int(socket, chunk_z)
    network.encode_boolean(socket, ground_up_continuous)
    network.encode_varint(socket, primary_bit_mask)
    network.encode_varint(socket, size)
    network.encode_byte_array(socket, data)
    network.encode_varint(socket, number_of_block_entities)
    # Should send an array of NBT tags here instead
    # network.encode_byte_array(socket, block_entities)


@encode_packet_header(PacketID.SEND_JOIN_GAME)
def send_join_game(socket, entity_id, gamemode, dimension, difficulty, max_players, level_type, reduced_debug_info):
    network.encode_int(socket, entity_id)
    network.encode_unsigned_byte(socket, gamemode)
    network.encode_int(socket, dimension)
    network.encode_unsigned_byte(socket, difficulty)
    network.encode_unsigned_byte(socket, max_players)
    network.encode_string(socket, level_type)
    network.encode_boolean(socket, reduced_debug_info)


@encode_packet_header(PacketID.SEND_PLAYER_ABILITIES)
def send_player_abilities(socket, flags, flying_speed, field_of_view_modifier):
    network.encode_byte(socket, flags)
    network.encode_float(socket, flying_speed)
    network.encode_float(socket, field_of_view_modifier)


# TODO: currently only adds or removes a single player to the player list
@encode_packet_header(PacketID.SEND_PLAYER_LIST_ITEM)
def send_player_list_item(socket, action, number_of_players, item_uuid, name=None, number_of_properties=None,
                          property_name=None, property_value=None, property_is_signed=None, gamemode=None, ping=None,
                          has_display_name=None):
    network.encode_varint(socket, action)
    network.encode_varint(socket, number_of_players)
    network.encode_uuid(socket, item_uuid)
    if action == 0:
        network.encode_string(socket, name)
        network.encode_varint(socket, number_of_properties)
        if number_of_properties > 0 and property_name is not None and property_value is not None and property_is_signed is not None:
            network.encode_string(socket, property_name)
            network.encode_string(socket, property_value)
            network.encode_boolean(socket, property_is_signed)
        network.encode_varint(socket, gamemode)
        network.encode_varint(socket, ping)
        network.encode_boolean(socket, has_display_name)
    elif action == 4:
        pass


@encode_packet_header(PacketID.SEND_PLAYER_POSITION_AND_LOOK)
def send_player_position_and_look(socket, x, y, z, yaw, pitch, flags, teleport_id):
    network.encode_double(socket, x)
    network.encode_double(socket, y)
    network.encode_double(socket, z)
    network.encode_float(socket, yaw)
    network.encode_float(socket, pitch)
    network.encode_byte(socket, flags)
    network.encode_varint(socket, teleport_id)


@encode_packet_header(PacketID.SEND_SPAWN_POSITION)
def send_spawn_position(socket, position):
    network.encode_position(socket, position)


# Play (Serverbound)


def recv_teleport_confirm(socket):
    teleport_id = network.decode_varint(socket)
    return teleport_id


def recv_chat_message(socket):
    chat_message = network.decode_string(socket)
    return chat_message


def recv_client_status(socket):
    action = network.decode_varint(socket)
    return action


def recv_keep_alive(socket):
    pong = network.decode_long(socket)
    return pong


def recv_player_position(socket):
    x = network.decode_double(socket)
    y = network.decode_double(socket)
    z = network.decode_double(socket)
    on_ground = network.decode_boolean(socket)
    return x, y, z, on_ground


def recv_player_position_and_look(socket):
    x = network.decode_double(socket)
    y = network.decode_double(socket)
    z = network.decode_double(socket)
    yaw = network.decode_float(socket)
    pitch = network.decode_float(socket)
    on_ground = network.decode_boolean(socket)
    return x, y, z, yaw, pitch, on_ground


def recv_player_look(socket):
    yaw = network.decode_float(socket)
    pitch = network.decode_float(socket)
    on_ground = network.decode_boolean(socket)
    return yaw, pitch, on_ground


def recv_player_digging(socket):
    status = network.decode_varint(socket)
    location = network.decode_position(socket)
    face = network.decode_byte(socket)
    return status, location, face


def recv_held_item_change(socket):
    slot = network.decode_short(socket)
    return slot


def recv_creative_inventory_action(socket):
    slot = network.decode_short(socket)
    clicked_item = network.decode_slot(socket)
    return slot, clicked_item


def recv_animation(socket):
    hand = network.decode_varint(socket)
    return hand


def recv_player_block_placement(socket):
    location = network.decode_position(socket)
    face = network.decode_varint(socket)
    hand = network.decode_varint(socket)
    cursor_position_x = network.decode_float(socket)
    cursor_position_y = network.decode_float(socket)
    cursor_position_z = network.decode_float(socket)
    return location, face, hand, cursor_position_x, cursor_position_y, cursor_position_z


# Status (Clientbound)


@encode_packet_header(PacketID.SEND_RESPONSE)
def send_response(socket, json_response):
    network.encode_string(socket, json_response)


@encode_packet_header(PacketID.SEND_PONG)
def send_pong(socket, pong):
    network.encode_long(socket, pong)
    socket.shutdown(2)
    socket.close()


# Status (Serverbound)


def recv_request(socket):
    packet_size = network.decode_varint(socket)
    packet_id = network.decode_varint(socket)
    return packet_size, packet_id


def recv_ping(socket):
    packet_size = network.decode_varint(socket)
    packet_id = network.decode_varint(socket)
    ping = network.decode_long(socket)
    return packet_size, packet_id, ping


# Login (Clientbound)


@encode_packet_header(PacketID.SEND_LOGIN_SUCCESS)
def send_login_success(socket, player_uuid, player_username):
    network.encode_string(socket, player_uuid)
    network.encode_string(socket, player_username)


# Login (Serverbound)


def recv_login_start(socket):
    packet_size = network.decode_varint(socket)
    packet_id = network.decode_varint(socket)
    player_username = network.decode_string(socket)
    return packet_size, packet_id, player_username


# Private methods


def _varint_size(number):
    return 1 if number == 0 else int(log(number, 0x80)) + 1


def _packet_size(packet_id, *args):
    # logging.debug("packet: " + hex(packet_id))
    packet_size = _varint_size(packet_id)
    # logging.debug("init size: " + str(packet_size))
    for arg in args:
        if isinstance(arg, network.Value):
            packet_size += arg.size
            # logging.debug("value size: " + str(arg.size))
        elif isinstance(arg, str):
            length = len(arg)
            packet_size += _varint_size(length) + length
            # logging.debug("string size: " + str(_varint_size(length) + length))
        elif isinstance(arg, int):
            packet_size += _varint_size(arg)
            # logging.debug("int size: " + str(_varint_size(arg)))
        elif isinstance(arg, uuid.UUID):
            packet_size += 16
            # logging.debug("uuid size: " + str(16))
        elif isinstance(arg, bytes):
            packet_size += len(arg)
            # logging.debug("bytes size: " + str(len(arg)))
        elif isinstance(arg, type(None)):
            pass
    # logging.debug("final size: " + str(packet_size))
    return packet_size
