class Player(object):
    def __init__(self, socket, uuid, username='', ):
        self.socket = socket
        self.username = username
        self.uuid = uuid
        self.inventory = Inventory()

    def __str__(self):
        return self._username

    @property
    def socket(self):
        return self._socket

    @property
    def uuid(self):
        return self._uuid

    @property
    def username(self):
        return self._username

    @socket.setter
    def socket(self, value):
        self._socket = value

    @uuid.setter
    def uuid(self, value):
        self._uuid = value

    @username.setter
    def username(self, value):
        self._username = value


class Inventory(object):
    def __init__(self):
        self.selected_slot = 0
        self.slots = {}

    @property
    def selected_slot(self):
        return self._selected_slot

    @selected_slot.setter
    def selected_slot(self, value):
        self._selected_slot = value

    def add_slot(self, slot_number, item_id, item_count):
        self.slots[slot_number] = Slot(True, item_id, item_count)

    def get_slot(self, slot_number):
        return self.slots[slot_number] if slot_number in self.slots else Slot(True, 0, 0)


class Slot(object):
    def __init__(self, present=False, item_id=0, item_count=0, nbt=None):
        self.present = present
        self.item_id = item_id
        self.item_count = item_count
        self.nbt = nbt

    @property
    def present(self):
        return self._present

    @present.setter
    def present(self, value):
        self._present = value

    @property
    def item_id(self):
        return self._item_id

    @item_id.setter
    def item_id(self, value):
        self._item_id = value

    @property
    def item_count(self):
        return self._item_count

    @item_count.setter
    def item_count(self, value):
        self._item_count = value

    @property
    def nbt(self):
        return self._nbt

    @nbt.setter
    def nbt(self, value):
        self._nbt = value
