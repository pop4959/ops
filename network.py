import struct
import logging
from sys import exit
from enum import Enum
import packet
from server import players


# Represent values of fixed data width


class Width(Enum):
    BYTE = '>b'
    UNSIGNED_BYTE = '>B'
    SHORT = '>h'
    UNSIGNED_SHORT = '>H'
    INT = '>i'
    LONG = '>q'
    UNSIGNED_LONG = '>Q'
    FLOAT = '>f'
    DOUBLE = '>d'
    ANGLE = '>b'

    def __str__(self):
        return self.value


class Value(object):
    def __init__(self, width, value):
        self.type = width.value
        self.value = value
        self.data = struct.pack(self.type, self.value)
        self.size = struct.calcsize(self.type)


# noinspection PyMissingConstructor
class Position(Value):
    def __init__(self, value):
        self.type = Width.UNSIGNED_LONG.value
        self.value = value
        self.x = value[0]
        self.y = value[1]
        self.z = value[2]
        self.data = struct.pack(self.type,
                                ((self.x & 0x3FFFFFF) << 38) | ((self.y & 0xFFF) << 26) | (self.z & 0x3FFFFFF))
        self.size = struct.calcsize(self.type)


# Encode or decode data types


MAX_VARINT_SIZE = 5
MAX_VARLONG_SIZE = 10


def decode_boolean(socket):
    return ord(_read_bytes(socket, 1)) | 0x00 != 0x00


def encode_boolean(socket, value):
    _write_bytes(socket, b'\x00' if value is False else b'\x01')


def decode_byte(socket):
    return struct.unpack(str(Width.BYTE), _read_bytes(socket, 1))[0]


def encode_byte(socket, value):
    _write_bytes(socket, value.data)


def decode_unsigned_byte(socket):
    return struct.unpack(str(Width.UNSIGNED_BYTE), _read_bytes(socket, 1))[0]


def encode_unsigned_byte(socket, value):
    _write_bytes(socket, value.data)


def decode_short(socket):
    return struct.unpack(str(Width.SHORT), _read_bytes(socket, 2))[0]


def encode_short(socket, value):
    _write_bytes(socket, value.data)


def decode_unsigned_short(socket):
    return struct.unpack(str(Width.UNSIGNED_SHORT), _read_bytes(socket, 2))[0]


def encode_unsigned_short(socket, value):
    _write_bytes(socket, value.data)


def decode_int(socket):
    return struct.unpack(str(Width.INT), _read_bytes(socket, 4))[0]


def encode_int(socket, value):
    _write_bytes(socket, value.data)


def decode_long(socket):
    return struct.unpack(str(Width.LONG), _read_bytes(socket, 8))[0]


def encode_long(socket, value):
    _write_bytes(socket, value.data)


def decode_float(socket):
    return struct.unpack(str(Width.FLOAT), _read_bytes(socket, 4))[0]


def encode_float(socket, value):
    _write_bytes(socket, value.data)


def decode_double(socket):
    return struct.unpack(str(Width.DOUBLE), _read_bytes(socket, 8))[0]


def encode_double(socket, value):
    _write_bytes(socket, value.data)


def decode_string(socket):
    string_length = decode_varint(socket)
    string_raw = _read_bytes(socket, string_length)
    return string_raw.decode('utf8')


def encode_string(socket, value):
    value = value.encode('utf8')
    encode_varint(socket, len(value))
    _write_bytes(socket, value)


# Chat


# Identifier


def decode_varint(socket):
    num_read = 0
    result = 0
    byte = 255
    while byte & 0b10000000 != 0:
        byte = ord(_read_bytes(socket, 1))
        value = byte & 0b01111111
        result |= value << (7 * num_read)
        num_read += 1
        if num_read > MAX_VARINT_SIZE:
            raise RuntimeError("VarInt is too large")
    if result & 0x80000000 != 0:
        result = -result
        result &= 0x8FFFFFFF
        result = -result
    return result


def encode_varint(socket, value):
    result = []
    if value == 0:
        result.append(0)
    else:
        while value != 0:
            temp = value & 0b01111111
            value >>= 7
            if value != 0:
                temp |= 0b10000000
            result.append(temp)
    _write_bytes(socket, bytes(result))


# def decode_varlong(socket):
#     pass


# def encode_varlong(socket, value):
#     pass


# Entity Metadata


# Slot


# Need to implement NBT when item support is improved
def decode_slot(socket):
    present = decode_boolean(socket)
    item_id = None
    item_count = None
    nbt = None
    if present:
        item_id = decode_varint(socket)
        item_count = decode_byte(socket)
        nbt = decode_byte(socket)
        if nbt != 0:
            logging.warning("DECODE SLOT: NBT NOT IMPLEMENTED BUT WAS NOT 0")
    return present, item_id, item_count, nbt


# def encode_slot(socket, value):
#     pass


# NBT Tag


def decode_position(socket):
    value = decode_long(socket)
    x = value >> 38
    y = (value >> 26) & 0xFFF
    z = value & 0x3FFFFFF
    if x >= 2 ** 25:
        x -= 2 ** 26
    if y >= 2 ** 11:
        x -= 2 ** 12
    if z >= 2 ** 25:
        z -= 2 ** 26
    return x, y, z


def encode_position(socket, value):
    _write_bytes(socket, value.data)


def decode_angle(socket):
    return decode_byte(socket)


def encode_angle(socket, value):
    _write_bytes(socket, value.data)


# def decode_uuid(socket):
#     return uuid.UUID(int=_read_bytes(socket, 16))


def encode_uuid(socket, value):
    uuid_int = value.int
    uuid_high = (uuid_int >> 64) & 0xFFFFFFFF
    uuid_low = uuid_int & 0xFFFFFFFF
    encode_long(socket, Value(Width.LONG, uuid_high))
    encode_long(socket, Value(Width.LONG, uuid_low))


# Optional X


# Array of X


# X Enum


def decode_byte_array(socket, size):
    return _read_bytes(socket, size)


def encode_byte_array(socket, value):
    _write_bytes(socket, value)


# Private methods


def _remove_player(socket):
    try:
        logging.warning(players[socket].username + " dropped connection")
        for player_socket in players:
            if player_socket is not socket:
                packet.send_player_list_item(player_socket, 4, 1, players[socket].uuid)
        del players[socket]
    except KeyError:
        logging.warning("Failed attempt to remove bad player socket")


def _read_bytes(socket, count):
    data = b''
    try:
        data = socket.recv(count)
    except ConnectionResetError:
        _remove_player(socket)
    try:
        if len(data) == 0:
            _remove_player(socket)
            socket.shutdown(2)
            socket.close()
            exit()
    except ConnectionResetError:
        _remove_player(socket)
        exit()
    return data


def _write_bytes(socket, data):
    if socket.fileno() > -1:
        try:
            socket.send(data)
        except ConnectionError:
            _remove_player(socket)
