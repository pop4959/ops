import time
import json
import logging
from threading import Thread
from sys import exit

import network
import packet
import server
from packet import PacketID
from server import players
from network import Width, Value, Position


def handle_chat_message(socket):
    chat_message = packet.recv_chat_message(socket)
    json_chat_message = json.dumps({"text": players[socket].username + ": " + chat_message})
    logging.info("Received chat message with contents " + json_chat_message)
    for player in players:
        packet.send_chat_message(player, json_chat_message, Value(Width.BYTE, 0))


def send_keep_alive(socket, threaded):
    if threaded:
        time.sleep(15)
    current_millis = int(round(time.time() * 1000))
    logging.debug("Trying to send keep alive")
    packet.send_keep_alive(socket, Value(Width.LONG, current_millis))
    if threaded:
        exit()


def handle_keep_alive(socket):
    pong = packet.recv_keep_alive(socket)
    Thread(target=send_keep_alive, args=(socket, True,)).start()
    logging.debug("Received keep alive from " + players[socket].username)


def handle_player_position(socket):
    x, y, z, on_ground = packet.recv_player_position(socket)
    # logging.debug("Movement (P) for " + players[socket].username + ". Position: (" + str(x) + "," + str(y) + "," + str(
    #     z) + "). On ground: " + str(on_ground) + ".")


def handle_player_position_and_look(socket):
    x, y, z, yaw, pitch, on_ground = packet.recv_player_position_and_look(socket)
    # logging.debug(
    #     "Movement (P&L) for " + players[socket].username + ". Position: (" + str(x) + "," + str(y) + "," + str(
    #         z) + "). Look: (" + str(yaw) + "," + str(pitch) + "). On ground: " + str(on_ground) + ".")


def handle_player_look(socket):
    yaw, pitch, on_ground = packet.recv_player_look(socket)
    # logging.debug(
    #     "Movement (L) for " + players[socket].username + ". Look: (" + str(yaw) + "," + str(
    #         pitch) + "). On ground: " + str(
    #         on_ground) + ".")


def handle_player_digging(socket):
    status, location, face = packet.recv_player_digging(socket)
    logging.debug(
        "Digging player " + players[socket].username + ": Status " + str(status) + ", Location " + str(
            location) + ", Face " + str(face))
    for player in players:
        packet.send_block_change(player, Position(location), 0)


def handle_held_item_change(socket):
    slot = packet.recv_held_item_change(socket)
    logging.debug("Player " + players[socket].username + " changed slot to " + str(slot))
    players[socket].inventory.selected_slot = slot


def handle_creative_inventory_action(socket):
    slot, clicked_item = packet.recv_creative_inventory_action(socket)
    present, item_id, item_count, nbt = clicked_item
    if present:
        players[socket].inventory.add_slot(slot, item_id, item_count)
        logging.info("Added " + str(item_id) + " (" + str(item_count) + ") to " + players[
            socket].username + "'s inventory at slot " + str(slot))


def handle_animation(socket):
    hand = packet.recv_animation(socket)
    logging.debug("Animation for " + players[socket].username + "'s " + ("main" if hand == 0 else "off") + " hand.")


def handle_player_block_placement(socket):
    location, face, hand, cursor_position_x, cursor_position_y, cursor_position_z = packet.recv_player_block_placement(
        socket)
    logging.debug("Block placed by " + players[
        socket].username + " at " + str(location) + " with face " + str(face) + " and hand " + str(
        hand) + " with cursor position " + str(cursor_position_x) + "," + str(cursor_position_y) + "," + str(
        cursor_position_z))
    # Handling the face offset manually
    x, y, z = location
    if face == 0:
        y -= 1
    elif face == 1:
        y += 1
    elif face == 2:
        z -= 1
    elif face == 3:
        z += 1
    elif face == 4:
        x -= 1
    elif face == 5:
        x += 1
    item_id = players[socket].inventory.get_slot(36 + players[socket].inventory.selected_slot).item_id
    try:
        block_id = server.item_id_to_block_id[item_id]
    except KeyError:
        block_id = 0
    for player in players:
        packet.send_block_change(player, Position((x, y, z)), block_id)


packet_handlers = {
    PacketID.RECV_CHAT_MESSAGE: handle_chat_message,
    PacketID.RECV_KEEP_ALIVE: handle_keep_alive,
    PacketID.RECV_PLAYER_POSITION: handle_player_position,
    PacketID.RECV_PLAYER_POSITION_AND_LOOK: handle_player_position_and_look,
    PacketID.RECV_PLAYER_LOOK: handle_player_look,
    PacketID.RECV_PLAYER_DIGGING: handle_player_digging,
    PacketID.RECV_HELD_ITEM_CHANGE: handle_held_item_change,
    PacketID.RECV_CREATIVE_INVENTORY_ACTION: handle_creative_inventory_action,
    PacketID.RECV_ANIMATION: handle_animation,
    PacketID.RECV_PLAYER_BLOCK_PLACEMENT: handle_player_block_placement
}


def listen(socket):
    send_keep_alive(socket, False)
    while 1:
        packet_size = network.decode_varint(socket)
        packet_id = network.decode_varint(socket)
        if packet_id in packet_handlers:
            packet_handlers.get(packet_id)(socket)
        elif packet_size > 0:
            payload = socket.recv(packet_size - 1)
            logging.debug("Unreceived packet id " + str(packet_id) + " / " + str(hex(packet_id)) + " with size " + str(
                packet_size) + " containing payload " + str(payload))
